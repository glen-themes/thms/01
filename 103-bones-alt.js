/*--------------------------------------------
death by rhizodont
--------------------------------------------*/

var customize_page = window.location.href.indexOf("/customize") > -1;
var on_main = window.location.href.indexOf("/customize") < 0;
    
// audio post play button - flaticon.com/free-icon/play-button_152770
var playb = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Layer_1' x='0px' y='0px' viewBox='0 0 330 330' style='enable-background:new 0 0 330 330;' xml:space='preserve'> <path id='XMLID_497_' d='M292.95,152.281L52.95,2.28c-4.625-2.891-10.453-3.043-15.222-0.4C32.959,4.524,30,9.547,30,15v300 c0,5.453,2.959,10.476,7.728,13.12c2.266,1.256,4.77,1.88,7.272,1.88c2.763,0,5.522-0.763,7.95-2.28l240-149.999 c4.386-2.741,7.05-7.548,7.05-12.72C300,159.829,297.336,155.022,292.95,152.281z M60,287.936V42.064l196.698,122.937L60,287.936z'/> </svg>";

document.documentElement.style.setProperty('--audioplay','url("' + playb + '")');

// audio post pause button - flaticon.com/free-icon/pause_747384
var pauseb = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Capa_1' x='0px' y='0px' viewBox='0 0 512 512' style='enable-background:new 0 0 512 512;' xml:space='preserve'> <g> <g> <path d='M154,0H70C42.43,0,20,22.43,20,50v412c0,27.57,22.43,50,50,50h84c27.57,0,50-22.43,50-50V50C204,22.43,181.57,0,154,0z M164,462c0,5.514-4.486,10-10,10H70c-5.514,0-10-4.486-10-10V50c0-5.514,4.486-10,10-10h84c5.514,0,10,4.486,10,10V462z'/> </g> </g> <g> <g> <path d='M442,0h-84c-27.57,0-50,22.43-50,50v412c0,27.57,22.43,50,50,50h84c27.57,0,50-22.43,50-50V50C492,22.43,469.57,0,442,0z M452,462c0,5.514-4.486,10-10,10h-84c-5.514,0-10-4.486-10-10V50c0-5.514,4.486-10,10-10h84c5.514,0,10,4.486,10,10V462z'/> </g> </g> </svg>";

document.documentElement.style.setProperty('--audiopause','url("' + pauseb + '")');

// audio post 'install audio' button
// feathericons
var cdrii = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='black' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-hash'><path d='M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4'></path><polyline points='7 10 12 15 17 10'></polyline><line x1='12' y1='15' x2='12' y2='3'></line></svg>";

document.documentElement.style.setProperty('--install','url("' + cdrii + '")');

// reblog icon
// flaticon.com/free-icon/exchange_1251615
var kjvwj = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.0' width='135.000000pt' height='135.000000pt' viewBox='0 0 135.000000 135.000000' preserveAspectRatio='xMidYMid meet'> <metadata> Created by potrace 1.16, written by Peter Selinger 2001-2019 </metadata> <g transform='translate(0.000000,135.000000) scale(0.100000,-0.100000)' fill='black' stroke='none'> <path d='M985 1320 l-29 -30 84 -85 85 -85 -421 0 c-244 0 -444 -4 -475 -10 -70 -14 -121 -49 -156 -107 -26 -44 -28 -58 -32 -170 l-3 -123 51 0 51 0 0 104 c0 90 3 107 23 137 46 70 41 69 532 69 l440 0 -88 -88 -87 -88 35 -34 35 -34 131 129 c71 72 132 141 136 154 5 20 -14 43 -128 157 -74 74 -139 134 -144 134 -6 0 -24 -14 -40 -30z'/> <path d='M1200 536 c0 -90 -3 -107 -23 -137 -46 -70 -41 -69 -532 -69 l-440 0 88 88 87 88 -35 34 -35 34 -135 -134 c-84 -83 -135 -142 -135 -155 0 -19 254 -285 273 -285 4 0 20 15 37 33 l29 32 -82 83 -82 82 421 0 c243 0 444 4 475 10 70 13 128 55 159 116 22 42 25 64 29 166 l3 118 -51 0 -51 0 0 -104z'/> </g> </svg>";

document.documentElement.style.setProperty('--rb-icon','url("' + kjvwj + '")');

// like icon
// flaticon.com/free-icon/white-heart_2267
var ftshhebo = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Capa_1' x='0px' y='0px' viewBox='0 0 15.848 15.848' style='enable-background:new 0 0 15.848 15.848;' xml:space='preserve'> <g> <path style='fill:black;' d='M15.135,2.469c-0.652-0.858-1.694-1.352-3.183-1.511c-0.119-0.013-0.243-0.02-0.371-0.02 c-1.32,0-2.819,0.655-3.658,1.892C7.085,1.593,5.586,0.938,4.267,0.938c-0.128,0-0.252,0.007-0.371,0.02 c-1.488,0.159-2.529,0.653-3.182,1.51c-0.647,0.85-0.856,2.016-0.62,3.467c0.349,2.126,3.19,4.799,5.473,6.947 c0.661,0.62,1.251,1.176,1.636,1.59c0,0,0.39,0.438,0.754,0.438c0.364,0,0.742-0.443,0.742-0.443 c0.383-0.415,0.972-0.973,1.631-1.598c2.262-2.145,5.077-4.813,5.424-6.935C15.99,4.485,15.782,3.319,15.135,2.469z M14.562,5.74 c-0.281,1.721-3.159,4.45-5.063,6.253c-0.62,0.589-1.152,1.093-1.55,1.511c-0.399-0.415-0.933-0.917-1.554-1.502 c-1.922-1.809-4.826-4.54-5.108-6.262C1.106,4.63,1.237,3.776,1.676,3.2c0.441-0.579,1.21-0.919,2.351-1.042 C4.103,2.15,4.184,2.146,4.268,2.146c1.02,0,2.752,0.642,3.062,2.363c0.052,0.287,0.302,0.496,0.594,0.496s0.543-0.209,0.594-0.496 c0.309-1.721,2.042-2.363,3.063-2.363c0.084,0,0.165,0.004,0.243,0.013c1.141,0.122,1.909,0.462,2.352,1.042 C14.613,3.775,14.743,4.63,14.562,5.74z'/> </g> </svg>";

document.documentElement.style.setProperty('--like-icon','url("' + ftshhebo + '")');

// hashtag icon
// feathericons
var hash = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='black' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-hash'><line x1='4' y1='9' x2='20' y2='9'></line><line x1='4' y1='15' x2='20' y2='15'></line><line x1='10' y1='3' x2='8' y2='21'></line><line x1='16' y1='3' x2='14' y2='21'></line></svg>";

// external link icon
// feathericons
document.documentElement.style.setProperty('--hashtag','url("' + hash + '")');

var schtd = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-external-link'><path d='M18 13v6a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h6'/><polyline points='15 3 21 3 21 9'/><line x1='10' y1='14' x2='21' y2='3'/></svg>";

document.documentElement.style.setProperty('--ext','url("' + schtd + '")');
    
// 'previous page' svg
// feathericons
var prev = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='black' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-hash'><polyline points='11 17 6 12 11 7'></polyline><polyline points='18 17 13 12 18 7'></polyline></svg>";

document.documentElement.style.setProperty('--BackSVG','url("' + prev + '")');
    
// 'next page' svg
// feathericons
var next = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='black' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-hash'><polyline points='13 17 18 12 13 7'></polyline><polyline points='6 17 11 12 6 7'></polyline></svg>";

document.documentElement.style.setProperty('--NextSVG','url("' + next + '")');
    
// calendar icon (permalink)
// flaticon.com/free-icon/cardio_3643555
var mk = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' id='Capa_1' enable-background='new 0 0 512 512' height='512' viewBox='0 0 512 512' width='512'><g><path d='m373.905 43v-30h-30v30h-175.81v-30h-30v30h-138.095v456h512v-456zm-235.81 30v30h30v-30h175.811v30h30v-30h108.094v60h-452v-60zm-108.095 396v-306h452v306z'/><path d='m271 348.315v-134.719l-88.107 88.764h-65.893v30h78.385l45.615-45.956v134.72l88.107-88.764h83.893v-30h-96.385z'/></g></svg>";

document.documentElement.style.setProperty('--calenglass','url("' + mk + '")');

// home icon
// flaticon.com/free-icon/home_1239343
var hovgazed = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' height='512pt' viewBox='0 -32 512.00021 512' width='512pt'><path d='m487.101562 146.617188-231.09375-146.617188-231.109374 146.625c-23.609376 13.914062-31.898438 44.566406-18.460938 68.492188 6.699219 11.921874 17.652344 20.472656 30.847656 24.074218 4.445313 1.210938 8.953125 1.8125 13.433594 1.8125 8.027344 0 15.949219-1.949218 23.21875-5.726562v212.390625h364.121094v-212.378907c11.328125 5.882813 24.242187 7.273438 36.640625 3.894532 13.191406-3.59375 24.144531-12.136719 30.847656-24.054688 13.460937-23.929687 5.179687-54.589844-18.445313-68.511718zm-191.179687 271.046874h-79.84375v-129.808593h79.84375zm30.007813 0v-159.816406h-139.859376v159.8125h-82.125v-201.480468l152.0625-98.035157 152.046876 98.027344v201.492187zm153.460937-217.242187c-2.730469 4.859375-7.203125 8.34375-12.582031 9.8125-5.285156 1.441406-10.796875.757813-15.5625-1.917969l-195.238282-125.871094-195.257812 125.878907c-4.765625 2.679687-10.277344 3.359375-15.566406 1.917969-5.382813-1.46875-9.851563-4.957032-12.582032-9.820313-5.503906-9.800781-2.078124-22.367187 7.644532-28.011719l.253906-.144531 215.507812-136.730469 215.242188 136.5625.503906.304688c9.726563 5.648437 13.152344 18.214844 7.636719 28.019531zm0 0'/></svg>";

document.documentElement.style.setProperty('--home-1239343','url("' + hovgazed + '")');

// glen svg
var cjaj = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.0' width='150.000000pt' height='150.000000pt' viewBox='0 0 150.000000 150.000000' preserveAspectRatio='xMidYMid meet'> <g transform='translate(0.000000,150.000000) scale(0.100000,-0.100000)' stroke='none'> <path d='M476 1267 c-249 -98 -363 -148 -373 -163 -11 -17 -14 -92 -14 -389 0 -363 1 -369 22 -391 20 -22 537 -234 570 -234 7 0 137 48 287 107 188 73 277 113 284 127 7 12 11 151 12 383 l1 364 105 42 c114 46 132 65 77 85 -61 22 -20 36 -600 -193 l-169 -66 -199 77 c-109 43 -198 79 -196 80 8 9 565 224 578 224 9 0 79 -25 155 -55 77 -30 149 -55 162 -55 12 0 39 7 59 15 32 13 35 17 22 30 -19 19 -372 155 -401 155 -13 -1 -184 -65 -382 -143z m-16 -336 c113 -45 214 -81 225 -81 11 0 123 41 248 90 126 50 230 90 233 90 2 0 3 -149 2 -331 l-3 -331 -200 -79 c-110 -43 -208 -82 -217 -85 -17 -5 -18 7 -18 165 0 208 4 202 -150 255 l-106 37 -47 -18 c-26 -10 -46 -22 -44 -28 2 -5 53 -28 113 -50 60 -22 115 -44 122 -48 18 -12 18 -320 0 -315 -7 2 -109 41 -225 88 l-213 85 0 332 0 332 38 -13 c20 -8 129 -50 242 -95z'/> </g> </svg>";

document.documentElement.style.setProperty('--glenSVG','url("' + cjaj + '")');

// corner musicplayer 'music note'
// flaticon.com/free-icon/musical-note-symbol_44871
var clfctwh = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Capa_1' x='0px' y='0px' width='38.178px' height='38.178px' viewBox='0 0 38.178 38.178' style='enable-background:new 0 0 38.178 38.178;' xml:space='preserve'> <g> <path d='M34.054,17.222c-0.211,0.189-0.522,0.199-0.747,0.028l-7.443-5.664l-3.526,21.095c-0.013,0.08-0.042,0.153-0.083,0.219 c-0.707,3.024-4.566,5.278-9.104,5.278c-5.087,0-9.226-2.817-9.226-6.28s4.138-6.281,9.226-6.281c2.089,0,4.075,0.467,5.689,1.324 l4.664-26.453c0.042-0.241,0.231-0.434,0.476-0.479c0.236-0.041,0.484,0.067,0.61,0.28L34.17,16.48 C34.315,16.726,34.266,17.033,34.054,17.222z'/> </g> </svg>";

document.documentElement.style.setProperty('--nota','url("' + clfctwh + '")');

// corner musicplayer 'play'
// flaticon.com/free-icon/play_748134
var uzhszvdj = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Capa_1' x='0px' y='0px' viewBox='0 0 511.999 511.999' style='enable-background:new 0 0 511.999 511.999;' xml:space='preserve'> <g> <g> <path d='M443.86,196.919L141.46,10.514C119.582-2.955,93.131-3.515,70.702,9.016c-22.429,12.529-35.819,35.35-35.819,61.041 v371.112c0,38.846,31.3,70.619,69.77,70.829c0.105,0,0.21,0.001,0.313,0.001c12.022-0.001,24.55-3.769,36.251-10.909 c9.413-5.743,12.388-18.029,6.645-27.441c-5.743-9.414-18.031-12.388-27.441-6.645c-5.473,3.338-10.818,5.065-15.553,5.064 c-14.515-0.079-30.056-12.513-30.056-30.898V70.058c0-11.021,5.744-20.808,15.364-26.183c9.621-5.375,20.966-5.135,30.339,0.636 l302.401,186.405c9.089,5.596,14.29,14.927,14.268,25.601c-0.022,10.673-5.261,19.983-14.4,25.56L204.147,415.945 c-9.404,5.758-12.36,18.049-6.602,27.452c5.757,9.404,18.048,12.36,27.452,6.602l218.611-133.852 c20.931-12.769,33.457-35.029,33.507-59.55C477.165,232.079,464.729,209.767,443.86,196.919z'/> </g> </g> </svg>";

document.documentElement.style.setProperty('--blay','url("' + uzhszvdj + '")');

// corner musicplayer 'pause'
// flaticon.com/free-icon/pause_748136
var nfuvx = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Capa_1' x='0px' y='0px' viewBox='0 0 512 512' style='enable-background:new 0 0 512 512;' xml:space='preserve'> <g> <g> <path d='M124.5,0h-35c-44.112,0-80,35.888-80,80v352c0,44.112,35.888,80,80,80h35c44.112,0,80-35.888,80-80V80 C204.5,35.888,168.612,0,124.5,0z M164.5,432c0,22.056-17.944,40-40,40h-35c-22.056,0-40-17.944-40-40V80 c0-22.056,17.944-40,40-40h35c22.056,0,40,17.944,40,40V432z'/> </g> </g> <g> <g> <path d='M482.5,352c11.046,0,20-8.954,20-20V80c0-44.112-35.888-80-80-80h-34c-44.112,0-80,35.888-80,80v352 c0,44.112,35.888,80,80,80h34c44.112,0,80-35.888,80-80c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20 c0,22.056-17.944,40-40,40h-34c-22.056,0-40-17.944-40-40V80c0-22.056,17.944-40,40-40h34c22.056,0,40,17.944,40,40v252 C462.5,343.046,471.454,352,482.5,352z'/> </g> </g> </svg>";

document.documentElement.style.setProperty('--bause','url("' + nfuvx + '")');

// scroll-to-top arrow
// flaticon.com/free-icon/arrow_1224323
var tvtfg = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' height='368pt' viewBox='-128 0 368 368.00296' width='368pt'><path d='m107.777344.945312c-2.601563-1.398437-5.75-1.230468-8.207032.402344l-43.570312 29.039063-43.558594-29.039063c-2.464844-1.632812-5.625-1.800781-8.207031-.402344-2.609375 1.394532-4.234375 4.105469-4.234375 7.058594v96c0 2.671875 1.335938 5.167969 3.5625 6.65625l44.4375 29.621094v195.722656l-33.597656-44.800781c-2.640625-3.527344-7.648438-4.25-11.199219-1.601563-3.539063 2.648438-4.25 7.664063-1.601563 11.195313l48 64c.097657.125.265626.183594.367188.308594.527344.625 1.160156 1.113281 1.855469 1.554687.28125.175782.511719.40625.808593.542969 1.039063.488281 2.160157.800781 3.367188.800781 1.210938 0 2.328125-.3125 3.363281-.792968.300781-.144532.527344-.367188.804688-.542969.699219-.441407 1.328125-.929688 1.859375-1.554688.101562-.125.269531-.175781.367187-.308593l48-64c2.65625-3.53125 1.933594-8.546876-1.601562-11.195313-3.535157-2.644531-8.542969-1.925781-11.199219 1.601563l-33.59375 44.792968v-195.722656l44.441406-29.621094c2.222656-1.488281 3.558594-3.984375 3.558594-6.65625v-96c0-2.953125-1.621094-5.664062-4.222656-7.058594zm-91.777344 98.777344v-76.777344l32 21.335938v76.769531zm80 0-32 21.328125v-76.769531l32-21.335938zm0 0'/></svg>";

document.documentElement.style.setProperty('--arrow-1224323','url("' + tvtfg + '")');

// =>

$(document).ready(function(){
    // check jquery version
    var jqver = jQuery.fn.jquery;
    jqver = jqver.replaceAll(".","");
    
    $(".tumblr_preview_marker___").remove();
    
    /*-------- TOOLTIPS --------*/
    $("a[title]:not([title=''])").style_my_tooltips({
        tip_follows_cursor:true,
        tip_delay_time:0,
        tip_fade_speed:69,
        attribute:"title"
    });
    
    /*------- REMOVE NPF VIDEO AUTOPLAY -------*/
    $("video[autoplay='autoplay']").each(function(){
        $(this).removeAttr("autoplay")
    });
    
    /*----------- NOSRC ADAPT -----------*/
    $(".nosrc").each(function(){
        if($(this).nextAll().find(".reblog-head").length){
            $(this).remove()
        } else {
            $(this).addClass("flex")
        }
    })
    
    /*----------- REMOVE <p> WHITESPACE -----------*/
    $(".postinner p").each(function(){
        if(!$(this).prev().length){
            if($(this).parent().is(".postinner")){

                $(this).css("margin-top",0)
            }
        }
        
        if(!$(this).next().length){
            // target last <p>
            // if it's empty, remove
            if($.trim($(this).html()) == ""){
                $(this).remove();
            }
        }
    })
    
    $(".postinner p, .postinner blockquote").each(function(){
        if(!$(this).next().length){
            // target last <p>
            // if no next sibling, negate bottom padding
            $(this).css("margin-bottom",0)
        }
        
        if($(this).next().is(".tagsdiv")){
            $(this).css("margin-bottom",0)
        }
    })
    
    /*----------- REBLOG-HEAD -----------*/
    $(".reblog-url").each(function(){
        var uz = $.trim($(this).text());
        if(uz.indexOf("-deac") > 0){
            var rogner = uz.substring(0,uz.lastIndexOf("-"));
            $(this).find("a").attr("href","//" + rogner + ".tumblr.com");
            $(this).find("a").text(rogner);
            $(this).append("<span class='deac'>(deactivated)</span>")
        }
    })
    
    /*-------- AUDIO BULLSH*T --------*/
    var mtn = Date.now();
    var fvckme = setInterval(function(){
        if(Date.now() - mtn > 1000){
            clearInterval(fvckme);
            $(".audiowrap").each(function(){
                $(this).prepend("<audio src='" + $(this).attr("audio-src") + "'>");
            });
            
            $(".inari").each(function(){
                var m_m = $(this).parents(".audiowrap").attr("audio-src");
                $(this).attr("href",m_m);
            })
        } else {
            $(".tumblr_audio_player").each(function(){
                if($(this).is("[src]")){
                    var audsrc = $(this).attr("src");
                    audsrc = audsrc.split("audio_file=").pop();
                    audsrc = decodeURIComponent(audsrc);
                    audsrc = audsrc.split("&")[0];
                    $(this).parents(".audiowrap").attr("audio-src",audsrc)
                }
            })
        }
    },0);
    
    $(".albumwrap").click(function(){
        
        var emp = $(this).parents(".audiowrap").find("audio")[0];
        
        if(emp.paused){
            emp.play();
            $(".overplay",this).addClass("ov-z");
            $(".overpause",this).addClass("ov-y");
        } else {
            emp.pause();
            $(".overplay",this).removeClass("ov-z");
            $(".overpause",this).removeClass("ov-y");
        }
        
        var that = this
        
        emp.onended = function(){
            $(".overplay",that).removeClass("ov-z");
            $(".overpause",that).removeClass("ov-y");
        }
    })
    
    // minimal soundcloud player Ãƒâ€šÃ‚Â© shythemes.tumblr
    var color = getComputedStyle(document.documentElement)
               .getPropertyValue("--Body-Text-Color");
    
    $('iframe[src*="soundcloud.com"]').each(function(){
        $(this).one("load",function(){
            soundfvk()
        });
    });
    
    function soundfvk(){
       $('iframe[src*="soundcloud.com"]').each(function(){
           $(this).attr({ src: $(this).attr('src').split('&')[0] + '&amp;liking=false&amp;sharing=false&amp;auto_play=false&amp;show_comments=false&amp;continuous_play=false&amp;buying=false&amp;show_playcount=false&amp;show_artwork=true&amp;origin=tumblr&amp;color=' + color.split('#')[1], height: 116, width: '100%' });
       });
    }
    
    $(".soundcloud_audio_player").each(function(){
        $(this).wrap("<div class='audio-soundcloud'>")
    })
    
    /*-------- QUOTE SOURCE BS --------*/
    $(".quote-source").each(function(){
        $(this).contents().filter(function(){
            return this.nodeType == 3 && this.data.trim().length > 0
        }).wrap("<span>");
        $(this).find("a.tumblr_blog").remove();
        
        $(this).find("span").each(function(){
            if($.trim($(this).text()) == "(via"){
                $(this).remove();
            }
            
            if($.trim($(this).text()) == ")"){
                $(this).remove();
            }
        })
        
        $(this).html(
            $(this).html().replace("(via","")
        )
        
        $(this).find("span:first").each(function(){
            if(!$(this).next().length){
                $(this).remove()
            }
        })
        
        $(this).find("p").each(function(){
            if($.trim($(this).text()) == ""){
                $(this).remove();
            }
            
            if($(this).children("br").length){
                if(!$(this).children().first().siblings().length){
                    $(this).remove()
                }
            }
            
            $(this).html($.trim($(this).html()));
            
            if($(this).text() == ")"){
                $(this).remove()
            }
        })
        
        $(this).find("p:last").each(function(){
            if($(this).find("a[href*='tumblr.com/post']").length){
                $(this).remove()
            }
        })
        
        $(this).find("p:last").each(function(){
            if(!$(this).next().length){
                $(this).css("margin-bottom",0)
            }
        })
    })
    
    $("[mdash] + p").each(function(){
        if(!$(this).next().length){
            if($.trim($(this).text()) !== ""){
                var sto = " " + $(this).html();
                $(this).prev().append(sto)
                $(this).remove();
            }
        }
    })
    
    /*-------- ASK/ANSWER POSTS --------*/
    $(".question_text").each(function(){
        if(!$(this).children().first().is("p")){
            $(this).wrapInner("<p></p>")
        }
    })
    
    /*-------- CHAT POSTS --------*/
    $(".npf_chat").each(function(){
        $(this).find("b").each(function(){
            var cb = $(this).html();
            $(this).before("<div class='chat_label'>" + cb + "</div>");
            $(this).remove()
        })
        
        $(this).contents().filter(function(){
            return this.nodeType == 3 && this.data.trim().length > 0
        }).wrap("<div class='chat_content'>");
        
        $(this).wrap("<div class='chat_row'>");
        $(this).children().unwrap()
    })
    
    $(".chat_row").each(function(){
        $(this).not(".chat_row + .chat_row").each(function(){
            if(jqver < "180"){
                $(this).nextUntil(":not(.chat_row").andSelf().wrapAll('<div class="chatwrap">');
            } else {
                $(this).nextUntil(":not(.chat_row").addBack().wrapAll('<div class="chatwrap">');
            }
        });
    })
    
    /*---- MAKE SURE <p> IS FIRST CHILD OF RB ----*/
    $(".reblog-head").each(function(){
        if(!$(this).next(".reblog-comment").length){
            $(this).nextUntil(".tagsdiv").wrapAll("<div class='reblog-comment'>")
        }
    })
    
    $(".reblog-comment").each(function(){
        if($(this).children().first().is("div")){
            $(this).prepend("<p></p>")
        }
    })
    
    
    
    /*-------- CLICKTAGS --------*/
    var tags_ms = parseInt(getComputedStyle(document.documentElement)
                   .getPropertyValue("--Tags-Fade-Speed-MS"));
    
    
    $(".clicktags").click(function(){
        var that = this;
        var tagsdiv = $(this).parents(".permadiv").prev(".postinner").find(".tagsdiv");
        
        if(!$(this).hasClass("clique")){
            $(this).addClass("clique");
            tagsdiv.slideDown(tags_ms);
            setTimeout(function(){
                tagsdiv.addClass("tagsfade");
            },tags_ms);
        } else {
            tagsdiv.removeClass("tagsfade");
            setTimeout(function(){
                tagsdiv.slideUp(tags_ms);
                $(that).removeClass("clique");
            },tags_ms)
        }
    })
    
    /*----------- POST NOTES -----------*/
    $("ol.notes a[title]").each(function(){
        $(this).removeAttr("title")
    });
    
    // remove tumblr redirects script by magnusthemes@tumblr
    // part 1/2
    $('a[href*="t.umblr.com/redirect"]').each(function(){
      var originalURL = $(this).attr("href").split("?z=")[1].split("&t=")[0];
      var replaceURL = decodeURIComponent(originalURL);
      $(this).attr("href", replaceURL);
    });
    
    // part 2/2
    function noHrefLi(){
        var linkSet = document.querySelectorAll('a[href*="href.li/?"]');
        Array.prototype.forEach.call(linkSet,function(el,i){
            var theLink = linkSet[i].getAttribute('href').split("href.li/?")[1];
            linkSet[i].setAttribute("href",theLink);
        });
    }
    noHrefLi();
    
    // make iframe heights look more 'normal'
	$(".embed_iframe").each(function(){
        if($(this).parent().is(".tmblr-embed")){
            var wut = $(this).width();
            
            var wrath = $(this).attr("width");
            var rat_w = wrath / wut;
            
            var hrath = $(this).attr("height");
            var rat_h = hrath / rat_w;
            
            $(this).height(rat_h)
        }
    })
    
    /*-------- HEADER STUFF --------*/
    var headh = getComputedStyle(document.documentElement)
               .getPropertyValue("--Header-Height");
               
    var contw = getComputedStyle(document.documentElement)
               .getPropertyValue("--ugh");
               
    if(customize_page){
        $("body").append("<div class='ghostest'></div>");
        $(".ghostest").css("width",contw);
        
        contw = $(".ghostest").width();
        
        $("body").append("<div class='msgbby'>your header dimensions are&ensp;<span>" + contw + "px</span> by <span>" + headh + "</span>.&ensp;To edit sidebar text, find the <span>'sidebar text'</span> textbox</div>")
    }
    
    /*-------- SIDEBAR STUFF --------*/
    var sidep = parseInt(getComputedStyle(document.documentElement).getPropertyValue("--Side-Padding"));
    var potatoe = parseInt(headh) + sidep;
               
    var scrollcont = $(".tout");
    var alitop = $(".le-sidebar").offset().top-sidep;
    var titop = $(".titbar").offset().top;
    
    /*--- fvck tvmblr ---*/
    var imgs = document.querySelectorAll("img");
    Array.prototype.forEach.call(imgs, function(invis){	
      if(invis.src.indexOf("assets.tumblr.com/images/x.gif") > -1){
        invis.setAttribute("src","https://cdn.glitch.com/bdf00c8f-434a-46d9-a514-ec8332ec176a/1x1.png");
      }
    });
               
    /*--------- SIDEBAR TEXT UHHH ---------*/
    var capico = getComputedStyle(document.documentElement)
               .getPropertyValue("--Sidebar-Text-Bullet");
               
    $(".sb-ico-text").each(function(){
        var sbtxt = $.trim($(this).html());
        $(this).html(
            $(this).html().replaceAll("* ","<i class='cp cp-" + capico + "'></i>")
        );
        
        $(this).contents().filter(function(){
            return this.nodeType === 3 && this.data.trim().length > 0
        }).wrap("<span>");
        
        $(this).find(".cp").each(function(){
            $(this).nextUntil(".cp").add($(this)).wrapAll("<div class='bullet-row'>");
        })
    })
    
    $(".bullet-row").each(function(){
        $(this).find(".cp").nextAll().wrapAll("<div class='bootext'>")
    })
    
    $(".bootext").each(function(){
        $(this).wrapInner("<div class='boing'>")
    })
    
    $(".boing").each(function(){
        $(this).find("span:first").each(function(){
            var spantxt = $.trim($(this).text());
            var naem = spantxt.split(":")[0];
            $(this).parents(".boing").attr("yup",naem);
            $(this).html(
                $(this).html().replace(":","").replace(naem,"<b>" + naem + ":</b>")
            )
        })
    })
    
    /*----- OTHER -----*/
    if(customize_page){
        $(".reblog-head img").each(function(){
            if($(this).attr("src") == $("html[portrait]").attr("portrait")){
                $(this).remove()
            }
        })
    }
    
    $(".chat_content").each(function(){
        if($.trim($(this).text()).indexOf("{block:") > -1){
            var notgod = $(this).html();
            notgod.replaceAll("{","&lcub;").replaceAll("}","&rcub;");
            $(this).before("<code>" + notgod + "</code>");
            $(this).remove()
        }
    })
    
    /*----- CHAD-----*/
    $(".circle-links").each(function(){
        if(!$(this).find("a[href*='glenthemes.tumblr.com']").length){
            $(".pancake").append("<a class='much-chad' href='//glenthemes.tumblr.com'>theme by glenthemes</a>");
            $(".much-chad").css({
                "display":"block",
                "margin-top":"10px",
                "text-align":"center",
                "font-family":"var(--SmallCaps-Font-Family)",
                "font-size":"calc(var(--SmallCaps-Font-Size) - 1.5px)",
                "text-transform":"uppercase",
                "letter-spacing":".5px",
                "color":"var(--Body-Text-Color)",
                "padding":"4px 5px"
            })
        }
    })
    
    /*---------- CORNER MUSIC PLAYER ----------*/
    if($(".mplayer").length){
      var aaa = document.getElementById("musique");

      aaa.volume = 0.69;

      if($("#musique").is("[autoplay]")){
          $(".pausee").addClass("aff");
          $(".playy").addClass("beff");
      }

      $(".music-controls").click(function(){
          if (aaa.paused) {
            aaa.play();
            $(".pausee").toggleClass("aff");
            $(".playy").toggleClass("beff");
          } else {
            aaa.pause();
            $(".playy").toggleClass("beff");
            $(".pausee").toggleClass("aff");
          }
      });

      aaa.onended = function(){
          $(".playy").removeClass("beff");
        $(".pausee").removeClass("aff");
      };

      $("#musique").each(function(){
          var mp3 = $.trim($(this).attr("src"));
          mp3 = mp3.replaceAll("?dl=0","").replaceAll("www.dropbox","dl.dropbox");
          $(this).attr("src",mp3)
      })
    }
	
	$(".caption").each(function(){
		if($.trim($(this).text()) == ""){
			$(this).remove();
		}
	});
    
    /*------- SCROLL TO TOP -------*/
    $(".raspberry").click(function(){
        scrollcont.animate({
            scrollTop:0
        },20);
    })
});//end jquery / end ready



/*------- SIDEBAR -------*/
// FloatSidebar.js by vursen
// github.com/vursen/FloatSidebar.js
const sidebarStuff = () => {
  if(typeof FloatSidebar == "function"){
    
    let sidePadding = parseInt(getComputedStyle(document.documentElement).getPropertyValue("--Side-Padding"));;
    let titbarHeight = document.querySelector(".titbar").offsetHeight;
    document.querySelector(":root").style.setProperty("--TIT-Height",`${titbarHeight}px`);
    
    let rsikl = Date.now();
    let zwbqe = setInterval(() => {
      if(Date.now() - rsikl > 21000){
        clearInterval(zwbqe);
        document.querySelector(":root").style.setProperty("--TIT-Height",`${document.querySelector(".titbar").offsetHeight}px`);
      } else {    
        document.querySelector(":root").style.setProperty("--TIT-Height",`${document.querySelector(".titbar").offsetHeight}px`);
        window.addEventListener("load", () => {
          clearInterval(zwbqe);
        })
      }
    },0)
    
    let topGap = sidePadding + titbarHeight;
    let botGap = sidePadding;
    let sidebar  = document.querySelector(".le-sidebar");
    let sidebarInner = sidebar.querySelector(".pancake");
    let relative = document.querySelector(".postscont");
    let viewport = document.querySelector(".tout")

    const floatSidebar = FloatSidebar({
      sidebar,
      sidebarInner,
      relative,
      topSpacing: topGap,
      bottomSpacing: botGap,
      viewport
    });
  }

}

document.addEventListener("DOMContentLoaded", () => {
  sidebarStuff();
})