![Text banner that says "Black Rabbit" in uppercase, and "Theme 01 by glenthemes" at the bottom](https://64.media.tumblr.com/cc75941ebe021b7eb2c0c2dde8e1e640/ef74af844cc27b32-16/s1280x1920/db5c0e72800251a8c735e4381834e7e34544a55b.png)  
![Screenshot preview of the theme "Black Rabbit" by glenthemes](https://64.media.tumblr.com/b0a1f0e4abb27cfc550321b814848f05/ef74af844cc27b32-57/s1280x1920/d5bb8e84015c43c00c2e0db8b2088282d27176e4.png)

**Theme no.:** 01  
**Theme name:** Black Rabbit  
**Theme type:** Free / Tumblr use  
**Description:** A header and sticky sidebar theme featuring Kirishima Ayato from Tokyo Ghoul.  
**Author:** @&hairsp;glenthemes  

**Release date:** [2015-07-17](https://64.media.tumblr.com/9b48cd37b10b4c4cae2bf829dd568022/tumblr_nrmwzjerYU1ubolzro3_r1_540.gif)  
**Rework date:** 2021-09-22

**Post:** [glenthemes.tumblr.com/post/152677161829](https://glenthemes.tumblr.com/post/152677161829)  
**Preview:** [glenthpvs.tumblr.com/black-rabbit](https://glenthpvs.tumblr.com/black-rabbit)  
**Download:** [pastebin.com/ZMMLqMbw](https://pastebin.com/ZMMLqMbw)  
**Credits:** [glencredits.tumblr.com/black-rabbit](https://glencredits.tumblr.com/black-rabbit)
