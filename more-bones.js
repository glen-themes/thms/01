// download button for npf audio

let npfAudio = ".npf-audio-wrapper";

waitForElement(npfAudio, { end: 3000 }).then(() => {
  document.querySelectorAll(npfAudio).forEach(npfAudio => {
    let aud = npfAudio.parentNode.querySelector("audio");
    if(aud){
      let ev = npfAudio.querySelector(".npf-audio-background");
      
      let ea = document.createElement("a");
      ea.classList.add("inari");
      ea.style.marginRight = "12px";
      
      let audSrc;
      
      if(aud.matches("[src]")){
        audSrc = aud.getAttribute("src");
      } else if(aud.querySelector("source[src]")){
        audSrc = aud.querySelector("source[src]").getAttribute("src")
      }
      
      if(audSrc){
        ea.href = audSrc;
        ea.target = "_blank"
        ea.title = "download"
      }
      
      ev.appendChild(ea);
      
      let en = document.createElement("span");      
      en.classList.add("int-all");
      
      ea.appendChild(en)
    }
  })
}).catch(err => {
  console.error(err)
})
